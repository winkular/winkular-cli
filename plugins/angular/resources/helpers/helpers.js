const fs = require('fs');

const elementTypes = {
    MODEL: 'model',
    SERVICE: 'service',
    LIST: 'list',
    DETAIL: 'detail',
}

const nonUpdateableModels = ['authenticationservicemodel', 'baseservicemodel', 'responseerror', 'usermedia', 'userservicemodel'];

const REGEXS = {
    fileTemplate: /\*\*(.+?)\*\*/gim,
    todoVerifyImports: /\/\/ TODO verify the following imports\: .+?;/g,
    endOfImports: /(import(?:.|\s)*?;\s+)((?:\/\/ TODO verify the following imports\: .+;\s+)*)((?:(?:\/{1,2}\*? ?(?:.|\s)+?(?:\/\/|\*\/|;)\s)+|\@NgModule|export class|export const))/m,
    modelPropDeclarations: /((?:\s+?[_wu]?id\??\: string\;){1,4})((?:(?:\r|\n|\r\n)\s*\w+\??(?: ?[\:\=] ?.*\;|\;))*)/m,
    serverModelMapMethods: /(static map(?:Reverse)? ?\((\w+?)\: (?:Server)?\w+\)\: (?:Server)?\w+? \{\s*?const (\w+?) ?\= ?.*?;((?:(?:\r|\n|\r\n)?\s*\3\.[_w]?id ?\= ?.*\2\..+){1,3};))((?:(?:\r|\n|\r\n)?\s*\3\.\w+ ?\= ?.*\2\..+)*)/gm,
    modelPropLine: /(\w+?)(\??)((?: ?\: ?.*)|$|(?: ?\= ?(.*)))/,
    // modelConstructor: /(constructor\s?\([_wu]?id\? ?\: ?string\,?){1,3}((?:(?:\r|\n|\r\n)?\s*\w+\??(?: ?\: ?.*\,?|\,|))*)(\s*\)\s?\{\s*)((?:\s*this\.\w+ ?= ?typeof \w+ ?\!\=\= ?\'undefined\' ?\? ? ?\w+ ?\: ?.*;)*)/m
    modelConstructor: /(constructor\s?\((?:[_wu]?id\? ?\: ?string\,?){1,3})((?:\s*\w+\??(?: ?\: ?.*\,?|\,|))*?)(\s*\)\s?\{\s*)((?:\s*?this\.[_wu]?id ?\= ?.+;){1,3})((?:\s*?this\.\w+ ?\= ?.*?;)*)/m,
    properNames: /\b[A-Z]\w*\b/gm
}

function getModulePaths(name, withExtensions = false) {
    const nameLowerCase = name.toLowerCase();
    const modelRoot = `src/app/modules/${nameLowerCase}`;
    let paths = {
        config: `${modelRoot}/${nameLowerCase}.conf`,
        modulePath: `${modelRoot}/${nameLowerCase}.module`,
        modelPath: `${modelRoot}/models/${name}`,
        serverModelPath: `${modelRoot}/models/Server${name}`,
        servicePath: `${modelRoot}/service/${nameLowerCase}.service`,
        detailPath: `${modelRoot}/${nameLowerCase}-detail/${nameLowerCase}-detail.component`,
        listPath: `${modelRoot}/${nameLowerCase}-list/${nameLowerCase}-list.component`
    }
    if (withExtensions) {
        for (let key in paths) {
            if (key === 'detailPath' || key === 'listPath') {
                paths[key + 'HTML'] = paths[key] + '.html';
                paths[key + 'SCSS'] = paths[key] + '.scss';
                paths[key + 'CSS'] = paths[key] + '.css';
                paths[key + 'CSSMAP'] = paths[key] + '.css.map';
            }
            paths[key] = key === 'config' ? paths[key] + '.json' : paths[key] + '.ts';
        }
    }
    return paths;
}

function validateName(name) {
    return typeof name === 'string' && /^[_a-zA-Z]{3,}$/.test(name);
}

function checkAlreadyExist(elementType, name) {
    let alreadyExists;
    const nameLowerCase = name.toLowerCase();
    switch(elementType) {
        case elementTypes.MODEL:
            alreadyExists = fs.existsSync(`src/app/modules/${nameLowerCase}/models/${name}.ts`) && fs.existsSync(`src/app/modules/${nameLowerCase}/models/Server${name}.ts`);
            break;
        case elementTypes.SERVICE:
            alreadyExists = fs.existsSync(`src/app/modules/${nameLowerCase}/service/${nameLowerCase}.service.ts`);
            break;
        case elementTypes.DETAIL:
            alreadyExists = fs.existsSync(`src/app/modules/${nameLowerCase}/${nameLowerCase}-detail/${nameLowerCase}-detail.component.ts`);
            break;
        case elementTypes.LIST:
            alreadyExists = fs.existsSync(`src/app/modules/${nameLowerCase}/${nameLowerCase}-list/${nameLowerCase}-list.component.ts`);
            break;
        default:
            alreadyExists = false;
    }
    return {
        alreadyExists,
        moduleExists: fs.existsSync(`src/app/modules/${nameLowerCase}/${nameLowerCase}.module.ts`) && fs.existsSync(`src/app/modules/${nameLowerCase}/${nameLowerCase}.conf.json`),
        modelAlreadyExists: fs.existsSync(`src/app/modules/${nameLowerCase}/models/${name}.ts`),
        serverModelAlreadyExists: fs.existsSync(`src/app/modules/${nameLowerCase}/models/Server${name}.ts`)
    };
}

module.exports = {
    elementTypes, getModulePaths, checkAlreadyExist, REGEXS, validateName, nonUpdateableModels
};