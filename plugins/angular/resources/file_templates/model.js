const modelTemplate = `import {Mappable} from '../../../@core/models/Mappable';
import {Server**ThisName**} from './Server**ThisName**';

export class **ThisName** implements Mappable<**ThisName**> {
  id: string;
  wid: string;

  constructor()
  constructor(id?: string) {
    this.id = typeof id !== 'undefined' ? id : null;
    this.wid = typeof id !== 'undefined' ? id : null;
  }

  map(obj: Server**ThisName**): **ThisName** {
    const u = Server**ThisName**.mapReverse(obj);
    console.log('###u', u);
    Object.keys(this).forEach(k => {
      this[k] = u[k];
    });
    return this;
  }

  mapReverse(): Server**ThisName** {
    return Server**ThisName**.map(this);
  }

}

`;

const serverModelTemplate = `import {**ThisName**} from './**ThisName**';

export class Server**ThisName** {
  _id?: string;
  wid?: string;

  static map(obj: **ThisName**): Server**ThisName** {
    const o = {} as Server**ThisName**;
    o._id = typeof obj.id !== 'undefined' ? obj.id : null;
    o.wid = typeof obj.id !== 'undefined' ? obj.id : null;
    return o;
  }

  static mapReverse(serverObject: Server**ThisName**): **ThisName** {
    const o = {} as **ThisName**;
    o.id = typeof serverObject._id !== 'undefined' ? serverObject._id : null;
    o.wid = typeof serverObject._id !== 'undefined' ? serverObject._id : null;
    return o;
  }
}

`;

module.exports.modelTemplate = modelTemplate;
module.exports.serverModelTemplate = serverModelTemplate;
